/**
 * Created by ionut.florea on 11.01.2017.
 */

'use strict';

var Application = require('spectron').Application;
var assert = require('assert');
var app = new Application({
    path: 'C:\\DPDSources\\Viking\\app\\shipping_app\\dist\\MyDPD-win32-x64\\MyDPD.exe',
    requireName: 'nodeRequire'
});



    app.start().then(function () {

        assert.equal(app.browserWindow.isVisible(), true);

        assert.equal( app.client.getTitle(), 'DPDGroup - Logon');


    }).catch(function (error) {
        console.error('Test failed', error.message)
    }).then(function () {
        return app.stop()
    });




