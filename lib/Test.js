/**
 * Created by ionut.florea on 11.01.2017.
 */
const webdriver = require('selenium-webdriver')

const driver = new webdriver.Builder()
// The "9515" is the port opened by chrome driver.
    .usingServer('http://localhost:9515')
    .withCapabilities({
        chromeOptions: {
            // Here is the path to your Electron binary.
            binary: './node_modules/.bin/chromedriver'
        }
    })
    .forBrowser('electron')
    .build()

driver.get('http://help.dpd.co.uk');
driver.findElement(webdriver.By.name('q')).sendKeys('webdriver')
driver.findElement(webdriver.By.name('btnG')).click()


driver.quit()